.PHONY: build
build:
	python3 setup.py sdist

.PHONY: upload
upload:
	zsh -c "python3 -m twine upload --verbose dist/pylogtail-*.tar.gz(n[-1])"

.PHONY: clean
clean:
	rm dist/*
