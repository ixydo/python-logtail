#!/usr/bin/env python3

from logtail import logtail
from time import sleep
from sys import argv

log = logtail(argv[1])

for line in log.readline():
    if line:
        print(line.rstrip())
    sleep(0.25)
